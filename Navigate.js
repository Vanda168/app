import { createStackNavigator } from 'react-navigation';

import App from './App';

const AppNavigator = createStackNavigator({
  App: { screen: App },
  Login: { screen: Login },
});