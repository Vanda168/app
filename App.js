import React, { useState } from 'react';
import axios from 'axios';
import { StyleSheet, Text, View, Button, TextInput, ScrollView} from 'react-native';
import Header from './layout/header';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function App() {

  [name,setName]=useState('');
  [note,setNote]=useState('');


  var tmp=<Text></Text>;

  const [body,setbody] = useState(<Text style={styles.item}></Text>);

  const saveData = () =>
    {

      axios({
        method: 'post',
        url: 'http://espackaging-group.com/API/insert.php',
        data: {
          name: name,
          note: note,
        }
      })
      .then(response => {
        alert(response);
        setName('');
        setNote('');
      })
      .catch(error => console.log(error))

      // fetch('http://espackaging-group.com/API/insert.php',
      // {
      //     method: 'POST',
      //     headers: 
      //     {
      //         'Accept': 'application/json',
      //         'Content-Type': 'application/json',
      //     },
      //     body: JSON.stringify(
      //     {
      //       name : name,
      //       note : note,
      //     })
      // }).then((response) => response.json()).then((responseJsonFromServer) =>
      // {
      //   alert(responseJsonFromServer);
      //   setName('');
      //   setNote('');
      // }).catch((error) =>
      // {
      //     // console.error(error);
      // });
    }

  const Remove= (selectid) =>{
    alert(selectid);
    console.log(selectid);
    fetch('http://espackaging-group.com/API/delete.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
          id : selectid,
        })
    }).then((response) => response.json()).then((responseJsonFromServer) =>
    {
      alert(responseJsonFromServer);
    }).catch((error) =>
    {
        // console.error(error);
    });
  }

  const getData1 = () =>{
    fetch('http://espackaging-group.com/API/select.php',{
      method:'POST',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json',
      },
      body: JSON.stringify({
        })
      })
    .then((response) => response.json())
    .then((res) => {
      tmp =  res.map(re=>{
        return  <View><Text style={styles.item}>{re.name}
                    <Text style={styles.detail_item_text}>{re.note==''?'':"\n"+re.note}</Text>
                </Text>
                    <Icon name='remove' style={styles.btnremove} onPress={() => Remove(re.id)}/>
                </View>;
      })
      setbody(tmp);
    })
    .catch((error)=>{
      alert(error.message);
    })
  }
  getData1(); 
  return (
    <View style={styles.container}>
      <Header/>
      <View style={styles.form}>
        <Text>Enter Name</Text>
        <TextInput 
          style={styles.text_input} 
          placeholder="Enter Name"
          onChangeText={(text) => setName(text)}
          value={name}
        />

        <Text>Enter Description</Text>
        <TextInput 
          style={styles.text_input} 
          placeholder="Enter Description"
          onChangeText={(text) => setNote(text)}
          value={note}
        />
        
      </View>
      <View style={styles.Buttonsize}>
        <Button title='Save' onPress={saveData}/>
      </View>
          <ScrollView>
            {body}
          </ScrollView>
    </View>
  );
  

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  text_input:{
    padding:7,
    borderWidth:1,
    borderColor:'silver',
  },
  item : {
    marginTop:10,
    padding:10,
    backgroundColor:'#FF7A1A',
    fontSize:20,
    fontWeight:'bold',
  },
  Buttonsize:{
    marginTop:10,
    marginBottom:10,
    width:100,
    height:30,
  },
  detail_item_text:{
    fontSize:10,
    fontWeight:'normal',
  },
  btnremove:{
    position:"absolute",
    right:10,
    top:27,
    fontSize:26,
    color:'white'
  }
});

